﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace VacationManCore.Services
{
    public interface ISender
    {
        Task<HttpResponseMessage> SendMessage(JObject message);
    }
}
