﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VacMan_Console.Controller
{
    public class Priority
    {
        public string Value { set; get; }
        
        public Priority(MessagePriority p)
        {
            if (p == MessagePriority.High)
            {
                Value = "High";
            }
            else if (p == MessagePriority.Normal)
            {
                Value = "Normal";
            }
        }

        public enum MessagePriority
        {
            High,
            Normal
        }
    }
    
}
