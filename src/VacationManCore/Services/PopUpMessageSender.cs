﻿using System.IO;
using System.Net;
using VacMan_Console.JObjects;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using System;

namespace VacationManCore.Services
{
    public class PopUpMessageSender : ISender
    {
        private const string FireBaseAddress = "https://fcm.googleapis.com/fcm/send";
        private const string ServerAPIKey = "AIzaSyCBXwP82lKpGDaOo5rCLCZVIJMREeA-T-o";
        public PopUpMessageSender()
        {
        }
        public async Task<HttpResponseMessage> SendMessage(JObject message)
        {
            HttpResponseMessage response;
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", $"key={ServerAPIKey}");
                client.BaseAddress = new Uri(FireBaseAddress);
             
                var requestMessage = new HttpRequestMessage()
                {
                    RequestUri = new Uri(FireBaseAddress),
                    Method = HttpMethod.Post
                };

                requestMessage.Content = new StringContent(message.ToString(), Encoding.UTF8,
                                    "application/json");
                response = await client.SendAsync(requestMessage);
            }
            return response;
        }

    }
}

