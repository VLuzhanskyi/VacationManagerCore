﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VacationManCore.Models
{
    public class UsersContext : DbContext
    {
        public int EmployeeID { set; get; }
        public string Email { set; get; }
        public string Password { set; get; }
        public string UserRole { set; get; }
    }
}
