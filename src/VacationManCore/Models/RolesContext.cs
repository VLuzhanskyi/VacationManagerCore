﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VacationManCore.Models
{
    public class RolesContext : DbContext
    {
        public int RoleID { set; get; }
        public string RoleName { set; get; }
    }
}
