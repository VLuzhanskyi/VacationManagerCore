﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VacationManCore.AbsenceTypes
{
    public class AbsenceStatusContext
    {
        public int AbsenceStatusID { set; get; }
        public string AbsenceStatusName { set; get; }
    }
}
