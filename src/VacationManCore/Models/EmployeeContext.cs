﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VacationManCore.Employee
{
    public class EmployeeContext : DbContext
    {
        public int EmployeeID { set; get; }

        public string FirstName { set; get; }

        public string LastName { set; get; }

        public string Email { set; get; }

        public string PhotoPath { set; get; }

        public int EmployeeType { set; get; }
    }
}
