﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VacationManCore.AbsenceTypes
{
    public class AbsenceTypeContext
    {
        public int AbsenceTypeID { set; get; }
        public string AbsenceTypeName { set; get; }
    }
}
