﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VacationManCore.AbsenceTypes
{
    public class AbsenceContext
    {
        public int AbsenceID { set; get; }
        public AbsenceTypeContext AbsenceType {set; get;}
        public int EmplyeeID { set; get; }
        public DateTime StartDate { set; get; }
        public DateTime EndDate { set; get; }
    }
}
