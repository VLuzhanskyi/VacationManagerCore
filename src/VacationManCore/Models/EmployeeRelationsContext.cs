﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VacationManCore.Models
{
    public class EmployeeRelationContext : DbContext
    {
        public int EmployeeID { set; get; }
        public int ManagerID { set; get; }
    }
}
