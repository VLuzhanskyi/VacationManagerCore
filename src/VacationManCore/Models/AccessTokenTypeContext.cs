﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VacationManCore.AccessToken
{
    public class AccessTokenTypeContext : DbContext
    {
        public int TypeID { set; get; }
        public string Name { set; get; }
    }
}
