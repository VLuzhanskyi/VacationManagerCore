﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VacationManCore.AbsenceTypes
{
    public class AbsenceStateHistoryContext : DbContext
    {
        public int AbsenceID { set; get; }
        public int ChangedBy { set; get; }
        public int AbsenceStatus { set; get; }
        public DateTime ChangedDate { set; get; }
        public string Comment { set; get; }
        public bool IsLatest { set; get; }
    }
}
