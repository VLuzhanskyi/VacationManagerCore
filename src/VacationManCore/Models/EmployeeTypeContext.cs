﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VacationManCore.Models
{
    public class EmployeeTypeContext : DbContext
    {
        public int TypeID { set; get; }
        public string TypeName { set; get; }
    }
}
