﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VacationManCore.Models
{
    public class EmplyeePushTokenContext : DbContext
    {
        public int EmployeeID { set; get; }
        public string PushToken { set; get; }
        public int TokenType { set; get; }
    }
}
