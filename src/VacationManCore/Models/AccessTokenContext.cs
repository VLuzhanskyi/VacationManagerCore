﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VacationManCore.AccessToken
{
    public class AccessTokenContext : DbContext
    {
        public int EmployeeID { set; get; }
        public string Token { set; get; }
        public DateTime ExpDate { set; get; }
        public int TokenType { set; get; }
    }
}
