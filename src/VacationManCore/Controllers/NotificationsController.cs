﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using VacationManCore.Services;
using System.Net.Http;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Text;
using Microsoft.AspNetCore.Authorization;

namespace VacationManCore.Controllers
{
    [Authorize]
    [Route("API/v1/Notifications")]
    public class NotificationsController : Controller
    {
        [HttpPost]
        public async void Post([FromBody]JObject data)
        {
            if (data != null && data.GetValue("notification", StringComparison.CurrentCultureIgnoreCase) != null)
            {
                var result = SendPopUpMessage(data);
                byte[] resultData = Encoding.UTF8.GetBytes(result.Result);
                await Response.Body.WriteAsync(resultData, 0, resultData.Length);
            }
        }

        private async Task<string> SendPopUpMessage(JObject data)
        {
            if (data != null)
            {
                PopUpMessageSender request = new PopUpMessageSender();
                var response = await request.SendMessage(data);
                string sResponseFromServer = await response.Content.ReadAsStringAsync();
                return string.Format("Status: {0}\n{1}", response.StatusCode.ToString(), sResponseFromServer);
            }
            else
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest).ToString();
            }
        }
    }
}
