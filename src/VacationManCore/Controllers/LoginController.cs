﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using VacationManCore.AccessToken;
using VacationManCore.Models;
using VacationManCore.Services;

namespace VacationManCore.Controllers
{
    [Authorize]
    [Route("API/v1/login")]
    public class LoginController : Controller
    {
        private static readonly string secretKey = "TEST_SecretKey!123";
        readonly Logger _log = LogManager.GetCurrentClassLogger();
        JToken username;
        JToken userPassword;
        JToken accessToken;
        JToken refreshToken;

        [HttpPost]
        public async void Post([FromBody]JObject data, [FromHeader]string token)
        {
            JObject responce;
            if (data != null)
            {
                _log.Debug("Reseived data: /n" + data.ToString());
                data.TryGetValue("email", StringComparison.CurrentCultureIgnoreCase, out username);
                data.TryGetValue("password", StringComparison.CurrentCultureIgnoreCase, out userPassword);

                if (token == null || TokenProvider.varifyToken((string)username, token)) //Or is expired
                {
                    responce = GenarateToken(data);
                    _log.Debug(responce.ToString());
                    responce.TryGetValue("token", StringComparison.CurrentCultureIgnoreCase, out accessToken);
                    responce.TryGetValue("refreshToken", StringComparison.CurrentCultureIgnoreCase, out refreshToken);
                    await sendResponce(responce);
                }
                else
                {
                    accessToken = token;
                }
                if (username == null || userPassword == null || accessToken == null)
                {
                    _log.Fatal("Login Fail!");
                }
                else
                {
                    _log.Info("Login Successful!");
                }
            }
            else
            {
                _log.Warn("Reseived data: null");
                _log.Error("Incorrect request received");
            }
        }
        [HttpPost]
        [Route("API/v1/login/refresh")]
        public async void Refresh([FromBody]JObject data)
        {
            JObject responce;
            if (data != null)
            {
                _log.Debug("Reseived data: /n" + data.ToString());
                data.TryGetValue("email", StringComparison.CurrentCultureIgnoreCase, out username);
                data.TryGetValue("password", StringComparison.CurrentCultureIgnoreCase, out userPassword);

                responce = GenarateToken(data);
                _log.Debug(responce.ToString());
                responce.TryGetValue("token", StringComparison.CurrentCultureIgnoreCase, out accessToken);
                responce.TryGetValue("refreshToken", StringComparison.CurrentCultureIgnoreCase, out refreshToken);
                await sendResponce(responce);

                if (username == null || userPassword == null || accessToken == null)
                {
                    _log.Fatal("Login Fail!");
                }
                else
                {
                    _log.Info("Login Successful!");
                }
            }
            else
            {
                _log.Warn("Reseived data: null");
                _log.Error("Incorrect request received");
            }
        }
        private async Task sendResponce(JObject message)
        {
            var jsonString = message.ToString();
            byte[] data = Encoding.UTF8.GetBytes(jsonString);
           
            if (jsonString.Contains("token"))
            {
               Response.StatusCode = 200;
               Response.Headers.Add("description", new Microsoft.Extensions.Primitives.StringValues("User successfully authorized"));
                Response.ContentType = new MediaTypeHeaderValue("application/json").ToString();
            }
            else if (jsonString.Contains("ReasonID=1")) 
            {
                Response.StatusCode = 401;
                Response.Headers.Add("description", 
                    new Microsoft.Extensions.Primitives.StringValues("No or invalid authentication details are provided."));
                Response.ContentType = new MediaTypeHeaderValue("application/json").ToString();
            }
            else
            {
                Response.StatusCode = 501;
                Response.Headers.Add("description", new Microsoft.Extensions.Primitives.StringValues("Something went wrong"));
                Response.ContentType = new MediaTypeHeaderValue("application/json").ToString();
            }
            await Response.Body.WriteAsync(data, 0, data.Length);
        }
        private JObject GenarateToken(JObject data)
        {
            JObject result = new JObject();
            bool IsValidUser = true;
            //Add user validation
            if (IsValidUser)
            {
                var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey));
                var options = new TokenProviderOptions
                {
                    Audience = "Delphi",
                    Issuer = data.GetValue("email").ToString(),
                    Expiration = new TimeSpan(183, 0, 0, 0),
                    SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.Sha256)
                };
                try
                {
                    //TokenProvider provider = new TokenProvider(Options.Create(options), data);
                    //result = provider.GenerateToken();
                    _log.Debug(string.Format("New Token Generated: {0}", result));
                } catch (Exception e)
                {
                    result.Add("ReasonID", 2);
                    result.Add("ReasonMessage", "Something went wrong");
                    _log.Error(e.ToString());
                }
            }
            else
            {
                result.Add("ReasonID", 1);
                result.Add("ReasonMessage", "User doesn't exist");
                _log.Error(result.ToString());
            }
            return result;
        }
        private JObject RefreshToken(JObject data, string refreshToken)
        {
            JObject result = new JObject();
            bool IsValidUser = true;
            //Add user validation
            if (IsValidUser)
            {
                var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey));
                var options = new TokenProviderOptions
                {
                    Audience = "Delphi",
                    Issuer = data.GetValue("email").ToString(),
                    Expiration = new TimeSpan(183, 0, 0, 0),
                    SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.Sha256)
                };
                try
                {
                   // TokenProvider provider = new TokenProvider(Options.Create(options), data);
                   // result = provider.GenerateToken();
                }
                catch (Exception e)
                {
                    result.Add("ReasonID", 2);
                    result.Add("ReasonMessage", "Something went wrong");
                    _log.Error(e.ToString());
                }
            } else
            {
                 result.Add("ReasonID", 1);
                result.Add("ReasonMessage", "User doesn't exist");
                _log.Error(result.ToString());
            }
            
            return result;
        }
    }
}
