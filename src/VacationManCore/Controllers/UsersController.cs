﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VacationManCore.Models;

namespace VacationManCore.Controllers
{
    [Authorize]
    [Route("API/v1/Users")]
    public class UsersController : Controller
    {
        [HttpGet]
        public User Get([FromHeader]string token)
        {
            return new User();
        }

        [HttpGet("{id}", Name = "GetAbsentes")]
        public List<Absence> GetUserAbsentes([FromHeader]string token, [FromBody]JObject data)
        {
            return new List<Absence>();
        }

        [HttpPost]
        public void AddApsentes([FromHeader]string token)
        {

        }
    }
}
