﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using VacationManCore.Models;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace VacationManCore.Controllers
{
    [Authorize]
    [Route("API/v1/absence")]
    public class AbsenceController : Controller
    {
        Absence a;
        public AbsenceController()
        {
            a = new Absence();
        }
        [HttpPut("{id}", Name = "updateAbsence")]
        public void UpdateAbsenceStatus([FromHeader]string token, [FromBody]JObject data)
        {
            int absenceId = getAbsence(Request.Path.ToString());
            JToken status = data.GetValue("absenceStatus");
            if (status.ToString() == "Approved")
            {
                a.ApproveAbsence(token, absenceId);
            } else if (status.ToString() == "")
            {
                a.DeclineAbsence(token, absenceId);
            }
            
        }
        
        [HttpDelete("{id}", Name = "deleteAbsentes")]
        public void DeleteAbsence([FromHeader]string token)
        {
            int absenceId = getAbsence(Request.Path.Value);
        }

        [HttpGet]
        [Route("/managed")]
        public void GetManagedAbsences([FromHeader]string token)
        {
            List<Absence> userManagedAbsences = a.GetAllManagedAbsences(token);
            foreach (var absence in userManagedAbsences)
            {
                //create JObject for responce
            }
        }

        private int getAbsence(string request)
        {            
            var stringID = request.Substring(request.LastIndexOf("/") + 1);
            var id = stringID.Substring(0, stringID.LastIndexOf("%"));
            int result;
            int.TryParse(id, out result);
            return result;

        }

    }
}
