﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VacationManCore.Models
{
    public interface IUser
    {
        void AddUser(User item);
        string AccessToken { get; set; }
        string RefreshToken { get; set; }
        IEnumerable<Absence> GetAllVacations();
        User Find(string key);
        void Remove(string UserId);
        void Update(User item);
    }
    
}
