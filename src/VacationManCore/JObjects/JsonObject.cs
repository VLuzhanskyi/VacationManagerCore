﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VacMan_Console.Controller;

namespace VacMan_Console.JObjects

{
    public class JsonObject
    {
        public string Topic { set; get; }

        public JsonObject(string topic)
        {
            Topic = topic;
        }
        
        public object CreateJObject(bool isTest)
        {
            if (isTest)
            {
                Notification test_notification = new Notification()
                {
                    _body = "Itali vs. Macedonia",
                    _title = "3 to 2"
                };
                Test_JObject test_obj = new Test_JObject(test_notification, Priority.MessagePriority.High )
                {
                    _topic = Topic,
                    _score = "3x2",
                    _title = "ItalivsMacedonia",
                    _time = "11d10m",
                    _goals = new List<jObjectListItem>()
                    {
                        new jObjectListItem("Balotelli", "24"),
                        new jObjectListItem("Immobile", "75"),
                        new jObjectListItem("Immobile", "92"),
                        new jObjectListItem("Nestorovsky", "57"),
                        new jObjectListItem("Hasany", "59"),
                        new jObjectListItem("Hasany", "59")
                    }
                };
                return test_obj.GetJObject();
            }
            else
            {
                Notification notification = new Notification()
                {
                    _body = "Error",
                    _title = "Sysadmin failed"
                };

                ErrorJObject obj = new ErrorJObject(notification, Priority.MessagePriority.High)
                {
                    topic = Topic,
                    _trapID = "5001",
                    _trapType = "error"
                };
                return obj.GetJObject();
            }            
        }
    }
}
