﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VacMan_Console.Controller;

namespace VacMan_Console.JObjects
{
    public class Test_JObject
    {
        public string _title { set; get; }
        public string _score { set; get; }
        public string _time { set; get; }
        public string _topic { set; get; }
        public string priority { set; get; }
        private Notification notification { set; get; }
        public List<jObjectListItem> _goals;
        public Test_JObject(Notification _notification, Priority.MessagePriority _priority)
        {
            _goals = new List<jObjectListItem>();
            priority = new Priority(_priority).Value;
            notification = _notification;
        }
        public object GetJObject()
        {
            var jData = new
            {
                to = _topic,
                data = new
                {
                    title = _title,
                    score = _score,
                    time = _time,
                    goals = _goals.ToArray()
                },

                notification = new
                {
                    body = notification._body,
                    title = notification._title,
                    //  icon = "myicon"
                },
                priority = priority
            };
            return jData;
        }
    }
}
