﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VacMan_Console.JObjects
{
   public class jObjectListItem
    {
        public string name { set; get; }
        public string value { set; get; }
        public jObjectListItem(string _name, string _value)
        {
            name = _name;
            value = _value;
        }
    }
}
