﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VacMan_Console.Controller;

namespace VacMan_Console.JObjects
{
    public class ErrorJObject
    {
        public string _trapType { set; get; }
        public string _trapID { set; get; }
        public string topic { set; get; }
        private Notification notification { set; get; }
        private string priority { set; get; }

        public ErrorJObject(Notification _notification, Priority.MessagePriority _priority)
        {
            notification = _notification;
            priority = new Priority(_priority).Value;
        } 
        
        public object GetJObject()
        {
            var jData = new
            {
                to = topic,
                data = new
                {
                    trapID = _trapID,
                    trapType = _trapType
                },

                notification = new
                {
                    body = notification._body,
                    title = notification._title,
                    //  icon = "myicon"
                },
                priority = priority
            };
            return jData;
        }  
    }
}
