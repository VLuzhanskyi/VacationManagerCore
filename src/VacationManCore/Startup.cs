﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using VacationManCore.Services;
using NLog.Extensions.Logging;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using VacationManCore.Models;
using Microsoft.Extensions.Options;
using VacationManCore.AccessToken;

namespace VacationManCore
{
    public class Startup
    {
        private static readonly string secretKey = "TEST_SecretKey!123";

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);
                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                builder.AddApplicationInsightsSettings(developerMode: true);
            
            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddSingleton<ISender, PopUpMessageSender>();
            services.AddApplicationInsightsTelemetry(Configuration);
            
            services.AddSingleton(provider => {
                return Configuration;
            } );
            
            //services.AddAuthentication();
            //services.AddDistributedMemoryCache();
            services.AddOptions();

            services.AddMvc()
                .AddJsonOptions(jsonOptions =>
                {
                    jsonOptions.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
                    jsonOptions.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver();
                });
            services.AddRouting();
        }
       
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
{
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddNLog();
            env.ConfigureNLog("nlog.config");
            app.UseStaticFiles();
            var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey));
            var options = new TokenProviderOptions
            {
                Path = "/API/v1/login1/",
                Audience = "Delphi",
                Issuer = "test@example.com",
                Expiration = new TimeSpan(183, 0, 0, 0),
                SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256)
            };
            app.UseMiddleware<TokenProvider>(Options.Create(options));
            app.UseApplicationInsightsRequestTelemetry();
            
            var tokenValidationParameters = new TokenValidationParameters
            {             
                ValidateAudience = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = signingKey,
                ValidateIssuer = true,
                ValidIssuer = options.Issuer,
                ValidateLifetime = true
            };
            // app.UseOAuthValidation();


            app.UseJwtBearerAuthentication(new JwtBearerOptions
            {
                AutomaticAuthenticate = true,
                AutomaticChallenge = true,
                TokenValidationParameters = tokenValidationParameters

            });
            app.UseMvc(routers =>
            {
                routers.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            //====== only for test

            //app.Run(async (context) =>
            //{   

            //   string RequestID = await request.SendMessage(request.Message);
            //    context.Response.ContentType = "text/plain";
            //    var address = string.Join(",", app.ServerFeatures?.Get<IServerAddressesFeature>().Addresses.ToArray());
            //    await context.Response.WriteAsync($"Server URLs: {address} \n");

            //    //await context.Response.WriteAsync($"Content rot: {env.ContentRootPath} \n");
            //    //await context.Response.WriteAsync($"Web root: {env.WebRootPath} \n");
            //    //await context.Response.WriteAsync($"Environment: {env.EnvironmentName} \n");
            //   await context.Response.WriteAsync($"App name: {env.ApplicationName} \n");
            // await context.Response.WriteAsync();
            //});
            //app.UseApplicationInsightsExceptionTelemetry();


        }
    }
}
