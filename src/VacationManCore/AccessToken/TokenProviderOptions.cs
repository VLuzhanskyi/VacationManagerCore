﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;

namespace VacationManCore.AccessToken
{
    public class TokenProviderOptions
    {
        public string Path { set; get; } //Login
        public string Issuer { set; get; }
        public string Audience { set; get; }
        public TimeSpan Expiration { set; get; } = TimeSpan.FromDays(183);
        public SigningCredentials SigningCredentials { set; get; }
         
    }
}
