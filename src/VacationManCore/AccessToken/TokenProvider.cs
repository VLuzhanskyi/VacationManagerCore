﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;
using Newtonsoft.Json;
using System.Security.Principal;
using Newtonsoft.Json.Linq;
using System.IO;

namespace VacationManCore.AccessToken
{
    public class TokenProvider
    {
        private string _data;
        private readonly RequestDelegate _next;
        private readonly TokenProviderOptions _options;

        public TokenProvider(IOptions<TokenProviderOptions> options, RequestDelegate next)
        {
            _options = options.Value;
            _next = next;
            _data = _options.Issuer;
        }

        public Task Invoke(HttpContext context)
        {
            if (!context.Request.Path.Equals(_options.Path, StringComparison.Ordinal))
            { 
                return _next(context);
            }
            if (!context.Request.Method.Equals("POST") || !context.Request.ContentType.Equals("application/json"))
            {
                context.Response.StatusCode = 400;
                return context.Response.WriteAsync("Bad request");                
            }

            return GenerateToken(context);
        }

        public async Task GenerateToken(HttpContext context)
        {
            var result = new JObject();
            var streamData = new StreamReader(context.Request.Body).ReadToEnd();
            var data = JsonConvert.DeserializeObject(streamData, new JsonSerializerSettings {
                Formatting = Formatting.Indented });
            var user = JObject.FromObject(data).GetValue("email").ToString();
            //var username= user.GetValue("email").ToString();
            _options.Audience = user;
            // var password = _data.GetValue("password").ToString();
            var accessJwt = await GetToken(user, _options.Expiration);
            var refreshJwt = await GetToken(user, _options.Expiration.Add(new TimeSpan(92, 0, 0, 0)));
            //var accessToken = new JProperty("token", accessJwt.Id);
            //var accessExpiration = new JProperty("tokenExpDate", accessJwt.ValidTo);
            //var refreshToken = new JProperty("refreshToken", refreshJwt.Id);
            //var refreshTokenExpiraion = new JProperty("refreshTokenExpDate", refreshJwt.ValidTo);
            //result.Add(accessToken);
            //result.Add(accessExpiration);
            //result.Add(refreshToken);
            //result.Add(refreshTokenExpiraion);
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(accessJwt);
            var refreshedJwt = new JwtSecurityTokenHandler().WriteToken(refreshJwt);
            result.Add(new JProperty("token", encodedJwt));
            result.Add(new JProperty("tokenExpDate", accessJwt.ValidTo));
            result.Add(new JProperty("refreshToken", refreshedJwt));
            result.Add(new JProperty("refreshTokenExpDate", refreshJwt.ValidTo));
            context.Response.ContentType = "application/json";
            await context.Response.WriteAsync(result.ToString());                 
            //return result;
         }
         
        private async Task<JwtSecurityToken> GetToken(string username, TimeSpan expirationIndex)
         {
            var now = DateTime.Now;
            var claims = await GetIdentity(username);
            var jwt = new JwtSecurityToken(
               issuer: _options.Issuer,
               audience: _options.Audience,
               claims: claims.Claims,
               notBefore: now,
               expires: now.Add(expirationIndex),
               signingCredentials: _options.SigningCredentials);
            return jwt;
        }
        private Task<ClaimsIdentity> GetIdentity(string username)
        {
            //++++++++++++++To Do++++++++++++++++
            //Add user validation
            //Add correct data to the next line
            //+++++++++++++++++++++++++++++++++++
            return Task.FromResult(new ClaimsIdentity(new GenericIdentity(username), new Claim[] 
            {
                new Claim(JwtRegisteredClaimNames.Sub, username),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, DateTime.Now.ToUniversalTime().ToString(), ClaimValueTypes.Integer64)
            }));
        }

        public static bool varifyToken(string username, string token)
        {
            //Add Token Verification
            return true;
        }
    }
}
